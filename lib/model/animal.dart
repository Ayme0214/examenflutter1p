import 'package:flutter/cupertino.dart';

class Animal {
  final int id;
  final String name;
  final String descripcion;
  final String ubicacion;
  final String reino;
  final String clase;

  Animal({
    required this.id,
    required this.name,
    required this.descripcion,
    required this.ubicacion,
    required this.reino,
    required this.clase,
  });
}

