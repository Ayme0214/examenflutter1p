import 'package:examen_apmovil1_aymemacias/model/animal.dart';

import 'animal.dart';

class Backend {

  static final Backend _backend = Backend._internal();

  factory Backend() {
    return _backend;
  }

  Backend._internal();

  final _animales=[
     Animal(id: 1, name: 'Condor Andino', descripcion: 'El cóndor andino, también cóndor de los Cerros, cóndor de los Andes, o simplemente cóndor (Vultur gryphus)2​ es una especie de ave de la familia Cathartidae que habita en América del Sur. El orden al que pertenece su familia se encuentra en disputa. Se extiende por la cordillera de los Andes, cordilleras próximas a ella y las costas adyacentes de los océanos Pacífico y Atlántico. ', clase: 'Aves', reino: 'Animalia', ubicacion: 'Ecuador'),
      Animal(id: 2, name: 'Armadillo', descripcion: 'Los dasipódidos (Dasypodidae), conocidos comúnmente como armadillos, son una familia de mamíferos placentarios del orden Cingulata. Se caracterizan por poseer un caparazón dorsal formado por placas yuxtapuestas, ordenadas por lo general en filas transversales, con cola bastante larga y extremidades cortas. Habitan en América.',clase: 'Mamifero', reino: 'Animalia', ubicacion: 'Ecuador'),
      Animal(id: 3, name: 'Oso de Antojo', descripcion: 'l oso de anteojos (Tremarctos ornatus), oso andino, oso antifaz, oso sudamericano, oso frontino, ucumari y jukumari, es una especie de mamífero de la familia Ursidae.', clase: 'Mamifero', reino: 'Animalia', ubicacion: 'Ecuador'),
      Animal(id: 4, name: 'Tapir Andino', descripcion: 'El tapir andino, danta de montaña o danta de páramo2​ (Tapirus pinchaque) es una especie de mamífero perisodáctilo de la familia de los tapíridos. Es una de las cuatro especies de tapir existentes en América, y el único que vive fuera de las selvas tropicales en estado salvaje.',  clase: 'Mamifero', reino: 'Animalia', ubicacion: 'Ecuador'),
       Animal(id: 5, name: 'Delfín rosado', descripcion: 'El delfín rosado (Inia geoffrensis), también conocido como boto, bufeo, delfín del Amazonas o tonina, es una especie de mamífero cetáceo odontoceto de la familia Iniidae2​ en peligro de extinción. Se conocen dos subespecies: Inia geoffrensis geoffrensis e Inia geoffrensis humboldtiana, las cuales se distribuyen por la cuenca del Amazonas, la cuenca alta del río Madeira y la cuenca del Orinoco,',  clase: 'Mamifero', reino: 'Animalia', ubicacion: 'Ecuador'),
  ];


 List<Animal> getAnimales(){
   return _animales;
 }
 


}
